// -----------------------------------------------
// mapedit.h
// -----------------------------------------------
// Built-in map editor (totally l33t!)
// ----------------------------------------------- 
// By Kronoman - July 2003 - December 2003
// In loving memory of my father
// -----------------------------------------------

#ifndef MAPEDIT_H
#define MAPEDIT_H

#include <allegro.h>

#include "tmap.h" // tile map class
#include "gerror.h" // to show any error that may arise 
#include "misc_def.h"
#include "mytracer.h"
#include "backgbmp.h"


class CMapEditor
{
	public:
		CMapEditor();
		~CMapEditor();
		
		void start_map_editor();
		
	private:
		void map_editor_help_message();
		
		void redraw_the_tile_map(int layer, bool grid);
		
		void do_change_mouse_cursor();
		
		void kprint(char *msg); // helper for printing messages on screen


		int tx, ty; // current scroll in the map
		int ts; // current tile selected from the tile set (1..255)
		int tl; // current layer of map selected 0..MAP_LAYERS-1 (MAP_LAYERS in tmap.h)
		bool draw_grid; // draw grid?
		
		CTMap game_map; // game map
		CBackground background; // backgrounds availables
		
		// two bitmaps for internal use
		BITMAP *dbuffer; // doble buffer
		BITMAP *mcursor; // mouse cursor

		// helper function for text print
		int ykprint, ckprint;
		
		// debugger
		CMyTracer mtracer;
};

#endif


