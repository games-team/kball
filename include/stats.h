// -----------------------------------------------
// stats.h
// -----------------------------------------------
// Statistics of gameplay for KBall
// ----------------------------------------------- 
// By Kronoman 
// Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

#ifndef KBALL_STATS_H
#define KBALL_STATS_H

#include <allegro.h>

class CGameStats
{
	public:
		CGameStats();
		~CGameStats();
		
		void reset(); // reset stats
		
		
		void print(BITMAP *bmp, int y, int color_fg, int color_bg, FONT *f); // print stats
		
		void add_time(int h, int m, int s); // use this to update time, will keep it ok
		
		// time stats (total time of gameplay)
		int h, s, m;
		
		// score
		long int score;
		
		// balls lost
		int blost;
};

#endif

