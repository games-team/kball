// -----------------------------------------------
// qmenu.h
// -----------------------------------------------
// Quick menu system for my games/etc
// ----------------------------------------------- 
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

// From hell they came!! arghhhh!!

#ifndef QMENU_H
#define QMENU_H

#include <allegro.h>

#include <string>
#include <vector> // STL container for the menu items
#include <iterator>
using namespace std;

#include "control.h" // I use my generic controller to control the menu system ;^D

// how many beats by second should the menu manager handle ? (default = 30)
// this is used for double buffer stuff mainly, and also for timing the calls to the optional callback routine
#define BPS_OF_MENU_MANAGER 30

// This class models a quick menu system
class CQMenu
{
	public:
		CQMenu();
		~CQMenu();
		
		void clear_menu_list(); // resets menu list to empty
		
		int add_item_to_menu(string item); // adds a selectable item to the menu, returns the index of the item added, or -1 on error (menu full)
		int add_item_to_menu(char *item) { return add_item_to_menu(string(item)); };
		
		int do_the_menu(int selected); // starts the menu loop until the user selects one, will return the index of the item selected by the user (starts at 0).
		int do_the_menu(void) { return do_the_menu(0); } // overload
		
		// set functions
		void set_font_s(FONT *f); // sets font to render (selected)
		void set_font_ns(FONT *f); // sets font to render (not selected)
		
		void set_fg_color_ns(int fg); // sets fg color (not selected)
		void set_bg_color_ns(int bg); // sets bg color (not selected)
		void set_fg_color_s(int fg); // sets fg color (selected)
		void set_bg_color_s(int bg); // sets bg color (selected)
		void set_xywh(int x, int y, int w, int h); // set pos of the menu
		void set_to_bitmap(BITMAP *b);
		void set_back_bitmap(BITMAP *b);
		void set_callback_drawer(void (*c)(CQMenu &d, bool do_logic));
		
		// get functions
		FONT *get_font_s();
		FONT *get_font_ns();
		int get_fg_color_ns();
		int get_bg_color_ns();
		int get_fg_color_s();
		int get_bg_color_s();
		int get_menu_item_count();
		int get_x();
		int get_y();
		int get_w();
		int get_h();
		BITMAP *get_to_bitmap();
		BITMAP *get_back_bitmap();
		string get_menu_item_text(int item_index); // will return string of item 

		// timing helpers (global timer)
		int get_time_counter();
		unsigned long int  get_big_timer_counter();

		// the controller object is public so you can control it directly to suit your needs
		CController control; // controller for menu control		
		
		// some public values that control the display of the menu itself
		int item_y_separation; // pixels to separate each item in 'Y'
		int text_align; // alignment of text of items: default = left (normal), 1 = right, 2 = center around mx, 3 = justify in mw space 
	private:
		 FONT *menu_font_s; // pointer to font to render the menu items (selected)
		 FONT *menu_font_ns; // pointer to font to render the menu items (not selected)
		 
		 int menu_fg_color_ns; // color of foreground of the text (item NOT selected)
		 int menu_bg_color_ns; // color of background of the text (-1 for transparent) (item NOT selected)
		 
		 int menu_fg_color_s; // color of foreground of the text (item SELECTED)
		 int menu_bg_color_s; // color of background of the text (-1 for transparent) (item SELECTED)
		 
		 int mx, my, mw, mh; // x,y,w,h of the menu (position for drawing)
		 
		 BITMAP *to_bitmap; // bitmap where the menu must be drawn in each draw update (usually 'screen')
		 BITMAP *back_bitmap; // background bitmap of the menu, will be put at 0,0 of "to_bitmap" on each redraw
		 
		 // this is a callback function that can be called in each logic and draw update 
		 // will be pased a reference to the caller object (to get bitmap, etc)
		 // do_logic will have TRUE if you need to update the logic
		 // do_logic will have FALSE if you need to render the graphics
		 // this scheme lets you keep a constant rate of animations
		 void (*callback)(CQMenu &d, bool do_logic);
		 
		 // data contained
		 vector <string> menu_item_text; // item menu container
		 
};


#endif
