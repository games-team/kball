kball (0.0.20041216-12) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Update standards version to 4.6.1, no changes needed.
  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 19 Nov 2022 06:26:58 -0000

kball (0.0.20041216-11) unstable; urgency=medium

  * Team upload.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Use canonical VCS URI.
  * Remove obsolete menu file.
  * Fix FTCBFS: Pass C++ compiler as GCC.
    Thanks to Helmut Grohne for the patch. (Closes: #930402)

 -- Markus Koschany <apo@debian.org>  Sun, 31 May 2020 02:46:17 +0200

kball (0.0.20041216-10) unstable; urgency=medium

  * Team upload.
  * Build-Depend on liballegro4-dev instead of virtual liballegro4.2-dev.
  * Use dh_installdocs-arch override to avoid a file conflict with
    /usr/share/doc/kball.

 -- Markus Koschany <apo@debian.org>  Fri, 03 Jun 2016 09:38:29 +0200

kball (0.0.20041216-9) unstable; urgency=medium

  * Team upload.
  * wrap-and-sort -sa.
  * Use compat level 9 and require debhelper >= 9.
  * Declare compliance with Debian Policy 3.9.8.
  * Use canonical VCS-URI.
  * kball.desktop: Add keywords and a comment in German.
  * Update debian/copyright to copyright format 1.0.
  * Simplify debian/rules by using dh sequencer. (Closes: #821997)
  * debian/watch: Use version=4.
  * Make the build reproducible by adding reproducible-build.patch.
    Thanks to Reiner Herrmann for the report and patch. (Closes: #825588)
  * Build with hardening build flags. Add hardening.patch.
  * Ensure that kball can be built twice in a row.

 -- Markus Koschany <apo@debian.org>  Thu, 02 Jun 2016 07:28:10 +0200

kball (0.0.20041216-8) unstable; urgency=low

  * Team upload.
  * Include sys/stat.h in 06_homedir_game.patch and 07_homedir_editor.patch.
    Closes: #624978

 -- Evgeni Golov <evgeni@debian.org>  Sat, 07 May 2011 15:53:36 +0200

kball (0.0.20041216-7) unstable; urgency=low

  * Team upload.
  * Use source format 3.0 (quilt).
  * Fix compilation with GCC 4.5. (Closes: #565011)
    + new patch: gcc-4.5.patch
  * debian/control: Add ${misc:Depends}.
  * Bump Standards-Version to 3.9.1 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 14 Mar 2011 15:33:17 +0100

kball (0.0.20041216-6) unstable; urgency=low

  [ Barry deFreese ]
  * Add myself to uploaders.
  * 08_dont_strip.patch - Remove -s from LDFLAGS. (Closes: #437246).
  * 09_keypad_enter.patch - Allow keypad enter to select. (Closes: #501844).
  * Improve clean target. (Wasn't removing .o files).
  * Bump Standards Version to 3.8.1. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Mon, 06 Apr 2009 20:52:28 -0400

kball (0.0.20041216-5) unstable; urgency=low

  [ Miriam Ruiz ]
  * Modified building system for using quilt.
  * Upgraded standards version to 3.8.0.
    + Menu policy transition.

  [ Jon Dowland ]
  * update menu section to "Games/Action" for menu transition
    (thanks Linas Žvirblis)

  [ Cyril Brulebois ]
  * Added Vcs-Svn and Vcs-Browser fields in the control file.

  [ Barry deFreese ]
  * Add desktop file
  * Call dh_desktop
  * Add Homepage field in control
  * Bump debhelper build-dep to match compat
  * Make clean not ignore errors
  * Fix substvar source:Version

  [ Gerfried Fuchs ]
  * Fixed watch file including mangling of upstream version

 -- Miriam Ruiz <little_miry@yahoo.es>  Sat, 26 Jul 2008 18:10:19 +0200

kball (0.0.20041216-4) unstable; urgency=low

  * Fixed broken debian/watch file added in -3.
  * Updated control to depend on data >= instead of = version.

 -- Miriam Ruiz <little_miry@yahoo.es>  Tue, 10 Jan 2006 14:28:22 +0100

kball (0.0.20041216-3) unstable; urgency=low

  * Removed circular dependency between kball and kball-data.
  * Automatic fallback to windowed mode. Thanks to Idan Sofer
    <idan@idanso.dyndns.org> for the patch. Closes: #313033.
  * Ending screen should not block user now. Thanks to Idan Sofer
    <idan@idanso.dyndns.org> for the patch. Closes: #311372.
  * Modified dependencies.
  * Reduced optimisation level to -O2 according to suggestion in #323388.
  * Upgraded standards version to 3.6.2
  * Dirty Hack: Replaced fixed by int32_t in mapped_sphere_ex (sphermap.cpp),
    because otherwise I get an invalid conversion from 'long int*' to 'fixed*'
  * Added Watch file for uscan.

 -- Miriam Ruiz <little_miry@yahoo.es>  Fri, 16 Dec 2005 21:05:10 +0100

kball (0.0.20041216-2) unstable; urgency=low

  * Update due to changes in the way xlibs-static-* packages are handled.

 -- Miriam Ruiz <little_miry@yahoo.es>  Sun,  3 Jul 2005 18:52:48 +0000

kball (0.0.20041216-1) unstable; urgency=low

  * Initial Release. Closes: #311080.

 -- Miriam Ruiz <little_miry@yahoo.es>  Sat,  2 Apr 2005 19:59:43 +0000
