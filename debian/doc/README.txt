According to the author, the manuals are covered by the same license as the
rest of the program. The fact that the original OOo files from which the
PDFs are generated aren't included in the original distribution is due to
the fact that he has forgotten to include them, so he sent me through email
the files I'm including here.


-------------------- ORIGINAL EMAIL FOLLOWING --------------------

Por los manuales te referis a los .pdf ?
Estan generados desde OpenOffice.org, no se si eso se puede considerar
codigo fuente.
Tendrian que estar en el paquete del codigo fuente, salvo que me los
haya olvidado...
Todo el juego y los manuales estan con la licencia del archivo LICENSE
que es considerada una licencia Open Source por la FSF, uso esa porque
es mas corta y facil de entender que la GNU.
La licencia tambien esta en 
http://www.opensource.org/licenses/mit-license.php
Mmm, no recuerdo si inclui o no los archivos del manual en la ultima
distribucion.
Igual, los adjunto.

Saludos
